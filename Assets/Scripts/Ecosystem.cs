﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ecosystem : MonoBehaviour {
    public static int id;
    public static int reset;

    public static List<GameObject> allOrganisms;
    public static float MAX_SENSE = 10;
    public float maxX;
    public float maxY;
    public GameObject food;
    public GameObject organism;
    private int worldSeed;

    // Start is called before the first frame update
    void Start() {
        worldSeed = Random.Range(0, 1000);
        id = 0;
        reset = 0;
        for (int i = 0; i < 1000; i++) {
            Vector2 randPos = new Vector2(Random.Range(-maxX, maxX), Random.Range(-maxY, maxY));
            Instantiate(food, randPos, new Quaternion(0, 0, 0, 0));
        }
        InitializeWorld();
        InvokeRepeating("GrowFood", 3f, 3f);
        InvokeRepeating("CalcStats", 1f, 1f);
    }

    void InitializeWorld() {
        allOrganisms = new List<GameObject>();
        for (int i = 0; i < 50; i++) {
            Vector2 randPos = new Vector2(Random.Range(-maxX, maxX), Random.Range(-maxY, maxY));
            GameObject temp = Instantiate(organism, randPos, new Quaternion(90, 90, 90, 90));
            temp.GetComponent<Wander>().enabled = true;
            temp.GetComponent<FoodIntake>().enabled = true;
            temp.GetComponent<OrganismStats>().enabled = true;
            temp.GetComponent<Reproduce>().enabled = true;
            temp.GetComponent<OrganismStats>().health = 100;
            temp.GetComponent<CircleCollider2D>().enabled = true;
            temp.name = reset + "Organism" + id++;
            temp.GetComponent<FoodIntake>().ate = 0;
            if (reset != 0) {
                temp.GetComponent<Wander>().speed = organism.GetComponent<Wander>().speed + Random.Range(-organism.GetComponent<Wander>().speed * 0.2f, organism.GetComponent<Wander>().speed * 0.2f);
                temp.GetComponent<Wander>().sense = organism.GetComponent<Wander>().sense + Random.Range(-organism.GetComponent<Wander>().sense * 0.2f, organism.GetComponent<Wander>().sense * 0.2f);
                float currentScale = temp.transform.localScale.x;
                float scale = Random.Range(-currentScale * 0.2f, currentScale * 0.2f);
                temp.transform.localScale = new Vector3(currentScale + scale, 1, currentScale + scale);
            } else {
                temp.GetComponent<Wander>().speed = Random.Range(0.01f, 50f);
                temp.GetComponent<Wander>().sense = Random.Range(0.01f, 50f);
                float scale = Random.Range(0.01f, 10.0f);
                temp.transform.localScale = new Vector3(scale, 1, scale);
            }
            allOrganisms.Add(temp);
        }
    }

    void GrowFood() {
        for (int i = 0; i < 50; i++) {
            Vector2 randPos = new Vector2(Random.Range(-maxX, maxX), Random.Range(-maxY, maxY));
            Instantiate(food, randPos, new Quaternion(0, 0, 0, 0));
        }
    }

    void CalcStats() {
        if (allOrganisms.Count == 1) {
            organism = allOrganisms[0];
            Destroy(allOrganisms[0]);
            reset++;
            InitializeWorld();
        }
        float avgSpeed = 0;
        float avgScale = 0;
        float avgSense = 0;
        float maxSpeed = 0;
        float maxScale = 0;
        float maxSense = 0;
        float minSpeed = 1000;
        float minScale = 1000;
        float minSense = 1000;
        float avgEnergyLoss = 0;
        float maxHealth = 0;

        float orgSpeed;
        float orgScale;
        float orgSense;
        float orgHealth;
        foreach (GameObject org in allOrganisms) {
            orgSpeed = org.GetComponent<Wander>().speed;
            orgScale = org.GetComponent<OrganismStats>().transform.localScale.x;
            orgHealth = org.GetComponent<OrganismStats>().health;
            orgSense = org.GetComponent<Wander>().sense;

            avgSpeed += orgSpeed;
            avgScale += orgScale;
            avgSense += orgSense;
            avgEnergyLoss += org.GetComponent<OrganismStats>().energyLoss;

            if (orgSpeed > maxSpeed) {
                maxSpeed = orgSpeed;
            }
            if (orgScale > maxScale) {
                maxScale = orgScale;
            }
            if (orgSense > maxSense) {
                maxSense = orgSense;
            }
            if (orgSpeed < minSpeed) {
                minSpeed = orgSpeed;
            }
            if (orgScale < minScale) {
                minScale = orgScale;
            }
            if (orgSense < minSense) {
                minSense = orgSense;
            }
            if (orgHealth > maxHealth) {
                maxHealth = orgHealth;
            }
        }

        MAX_SENSE = maxSense;
        string outputLine = allOrganisms.Count + " " + Math.Round(avgSpeed / allOrganisms.Count, 2) + " " + Math.Round(avgScale / allOrganisms.Count, 2) + " "
            + Math.Round(avgSense / allOrganisms.Count, 2) + " " + Math.Round(avgEnergyLoss / allOrganisms.Count, 2) + " "
            + Math.Round(minSpeed, 2) + " " + Math.Round(minScale, 2) + " "
            + Math.Round(minSense, 2) + " " + Math.Round(maxSpeed, 2) + " "
            + Math.Round(maxScale, 2) + " " + Math.Round(maxSense, 2) + " \n";
        //File.AppendAllText("C:\\Users\\Uros\\Documents\\Outputs\\res" + worldSeed + ".txt", outputLine);
        File.AppendAllText("./output/" + worldSeed + ".txt", outputLine);
    }
}



