﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reproduce : MonoBehaviour {
    public GameObject myPrefab;

    // Start is called before the first frame update
    void Start() {
        InvokeRepeating("Multiply", Random.Range(5f, 7f), Random.Range(5f, 7f));
    }

    void Multiply() {
        GameObject offspring = Instantiate(myPrefab, transform.position + new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), 0), transform.rotation) as GameObject;
        offspring.GetComponent<Wander>().enabled = true;
        offspring.GetComponent<FoodIntake>().enabled = true;
        offspring.GetComponent<OrganismStats>().enabled = true;
        offspring.GetComponent<Reproduce>().enabled = true;
        offspring.GetComponent<CircleCollider2D>().enabled = true;

        offspring.GetComponent<Wander>().speed = gameObject.GetComponent<Wander>().speed += Random.Range(-gameObject.GetComponent<Wander>().speed * 0.05f, gameObject.GetComponent<Wander>().speed * 0.05f);
        offspring.GetComponent<Wander>().sense = gameObject.GetComponent<Wander>().sense += Random.Range(-gameObject.GetComponent<Wander>().sense * 0.05f, gameObject.GetComponent<Wander>().sense * 0.05f);
        float sizeDelta = Random.Range(-offspring.transform.localScale.x * 0.05f, offspring.transform.localScale.x * 0.05f);
        offspring.transform.localScale += new Vector3(sizeDelta, 0, sizeDelta);
        offspring.name = Ecosystem.reset + " Organism " + Ecosystem.id++;
        offspring.GetComponent<OrganismStats>().health = 100;
        offspring.GetComponent<FoodIntake>().ate = 0;

        Ecosystem.allOrganisms.Add(offspring);
    }
}
