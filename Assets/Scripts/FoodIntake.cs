﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodIntake : MonoBehaviour {

    [HideInInspector]
    public int ate = 0;
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "food") {
            GetComponent<OrganismStats>().health += 50;
            Destroy(other.gameObject);
            ate++;
        } else if (other.tag == "organism" && transform.localScale.x > other.transform.localScale.x * 1.2) {
            GetComponent<OrganismStats>().health += other.GetComponent<OrganismStats>().health * other.transform.localScale.x;
            Ecosystem.allOrganisms.Remove(other.gameObject);
            Destroy(other.gameObject);
            ate++;
        }
    }
}
