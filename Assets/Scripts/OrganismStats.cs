﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrganismStats : MonoBehaviour {

    private float MAX_HEALTH = 100;
    public float health;
    public GameObject food;
    private Renderer rend;
    [HideInInspector]
    public float energyLoss;
    private int constant = 20;
    private int divider = 7;


    // speed
    // scale
    // sense

    // Start is called before the first frame update
    void Start() {
        rend = GetComponent<Renderer>();
        InvokeRepeating("Metabolism", 2f, 0.5f);
    }

    void Metabolism() {
        energyLoss = (constant 
            + GetComponent<Wander>().speed * GetComponent<Wander>().speed 
            + transform.localScale.x * transform.localScale.x * transform.localScale.x
            + GetComponent<Wander>().sense)/divider;
        health -= (float)energyLoss;
        float colorIndex = GetComponent<Wander>().sense / (Ecosystem.MAX_SENSE);
        rend.material.color = new Color(1 - colorIndex, 0, colorIndex, 1);
        
        if (health > MAX_HEALTH) {
            health = MAX_HEALTH;
        }
        
        if (health <= 0) {
            if (Ecosystem.allOrganisms.Count != 1) {
                Ecosystem.allOrganisms.Remove(gameObject);
                GetComponent<Wander>().closestTarget = null;
                Destroy(gameObject);
            }
        }
    }
}
