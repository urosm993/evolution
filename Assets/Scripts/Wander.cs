﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour {
    public float maxX;
    public float maxY;
    [HideInInspector]
    public Transform closestTarget;
    [HideInInspector]
    public float speed;
    [HideInInspector]
    public float sense;
    private Vector2 destination;

    void Start() {
        destination = transform.position;
        closestTarget = null;
        InvokeRepeating("ChangeDestination", 1f, Random.Range(0.5f, 2f));
    }

    // Update is called once per frame
    void Update() {

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, sense);
        if (hitColliders.Length > 1) {
            float closestDistanceSqr = Mathf.Infinity;
            int i = 0;
            while (i < hitColliders.Length) {
                if ((hitColliders[i].transform.position != transform.position) &&
                    (hitColliders[i].gameObject.tag == "food" ||
                    hitColliders[i].gameObject.tag == "organism" && transform.localScale.x > hitColliders[i].gameObject.transform.localScale.x * 1.2) ||
                    hitColliders[i].gameObject.tag == "organism" && transform.localScale.x * 1.2 < hitColliders[i].gameObject.transform.localScale.x
                    ) {
                    Vector3 directionToTarget = hitColliders[i].transform.position - transform.position;
                    float dSqrToTarget = directionToTarget.sqrMagnitude;
                    if (dSqrToTarget < closestDistanceSqr) {
                        closestDistanceSqr = dSqrToTarget;
                        closestTarget = hitColliders[i].transform;
                    }
                }
                i++;
            }
        }

        int fleeKoef = 1;
        if (closestTarget != null) {
            if (closestTarget.gameObject.tag == "organism" && transform.localScale.x * 1.2 < closestTarget.localScale.x) {
                fleeKoef = -1;
            }
            transform.position = Vector2.MoveTowards(transform.position, fleeKoef * closestTarget.position, speed * Time.deltaTime);
        } else {
            RandomMovement();
        }
    }

    void RandomMovement() {
        transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);
        if (transform.position.x > maxX)
            transform.position = new Vector2(-maxX, transform.position.y);
        if (transform.position.x < -maxX)
            transform.position = new Vector2(maxX, transform.position.y);
        if (transform.position.y > maxY)
            transform.position = new Vector2(transform.position.x, -maxY);
        if (transform.position.y < -maxY)
            transform.position = new Vector2(transform.position.x, maxY);
    }


    void ChangeDestination() {
        destination = new Vector2(transform.position.x + Random.Range(-20, 20), transform.position.y + Random.Range(-20, 20));
    }
}
